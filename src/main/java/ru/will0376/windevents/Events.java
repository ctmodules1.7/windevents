package ru.will0376.windevents;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;
import ru.will0376.windevents.events.EventLogPrint;
import ru.will0376.windevents.events.EventModStatusResponse;

@Mod(
		modid = Events.MOD_ID,
		name = Events.MOD_NAME,
		version = Events.VERSION,
		acceptableRemoteVersions = "*"
)
public class Events {
	public static final String MOD_ID = "windevents";
	public static final String MOD_NAME = "WindEvents";
	public static final String VERSION = "1.0.0.0";

	@Mod.Instance(MOD_ID)
	public static Events INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {

	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, EnumChatFormatting.GOLD + "[Events]" + EnumChatFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, true));
	}
}

