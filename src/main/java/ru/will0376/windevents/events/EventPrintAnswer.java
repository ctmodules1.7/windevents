package ru.will0376.windevents.events;

import cpw.mods.fml.common.eventhandler.Event;

public class EventPrintAnswer extends Event {
	private final String playerNick;
	private final String adminNick;
	private final String text;
	private final boolean wbs;
	private final boolean report;

	public EventPrintAnswer(String text, String adminNick, String playerNick, boolean wbs, boolean report) {
		this.text = text;
		this.adminNick = adminNick;
		this.playerNick = playerNick;
		this.wbs = wbs;
		this.report = report;
	}

	public String getPlayerNick() {
		return playerNick;
	}

	public String getAdminNick() {
		return adminNick;
	}

	public boolean isWbs() {
		return wbs;
	}

	public boolean isReport() {
		return report;
	}

	public String getText() {
		return text;
	}
}
