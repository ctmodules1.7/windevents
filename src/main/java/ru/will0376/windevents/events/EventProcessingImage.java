package ru.will0376.windevents.events;


import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;

import java.awt.image.BufferedImage;

@Cancelable
public class EventProcessingImage extends Event {
	BufferedImage image;
	String admin;
	String player;
	boolean wbs;
	boolean report;

	public EventProcessingImage(BufferedImage image, String admin, String player, boolean wbs, boolean report) {
		this.image = image;
		this.admin = admin;
		this.player = player;
		this.wbs = wbs;
		this.report = report;
	}

	public BufferedImage getImage() {
		return image;
	}

	public String getAdmin() {
		return admin;
	}

	public String getPlayer() {
		return player;
	}

	public boolean isWbs() {
		return wbs;
	}

	public boolean isReport() {
		return report;
	}
}
