package ru.will0376.windevents.events;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.entity.player.EntityPlayer;

@Cancelable
public class EventRequestPrintScreenLog extends Event {
	EntityPlayer player;
	String text;
	int page = 0;

	public EventRequestPrintScreenLog(EntityPlayer player, String text) {
		this.player = player;
		this.text = text;
	}

	public EventRequestPrintScreenLog(EntityPlayer player, String text, int page) {
		this.player = player;
		this.text = text;
		this.page = page;
	}

	public int getPage() {
		return page;
	}

	public EntityPlayer getPlayer() {
		return player;
	}

	public String getText() {
		return text;
	}
}
