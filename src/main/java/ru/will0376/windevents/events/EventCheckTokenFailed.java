package ru.will0376.windevents.events;


import cpw.mods.fml.common.eventhandler.Event;

public class EventCheckTokenFailed extends Event {
	private final String adminNick;
	private final String playerNick;
	private final String token;
	private final String domain;

	public EventCheckTokenFailed(String adminNick, String playerNick, String token, String domain) {
		this.adminNick = adminNick;
		this.playerNick = playerNick;
		this.token = token;
		this.domain = domain;
	}

	public String getAdminNick() {
		return adminNick;
	}

	public String getPlayerNick() {
		return playerNick;
	}

	public String getToken() {
		return token;
	}

	public String getDomain() {
		return domain;
	}
}
