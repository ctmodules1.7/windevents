package ru.will0376.windevents.events;

import com.google.gson.JsonObject;
import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;

import java.net.InetAddress;

@Cancelable
public class EventReceiveScreen extends Event {

	private String playerNick;
	private String token;
	private String adminNick;
	private JsonObject jo;
	private boolean wbs;
	private boolean report;
	private InetAddress ia;

	public EventReceiveScreen(JsonObject jo, InetAddress ia) {
		try {
			this.ia = ia;
			this.jo = jo;
			token = jo.get("token").getAsString();
			playerNick = jo.get("player").getAsString();
			adminNick = jo.get("admin").getAsString();
			wbs = jo.get("wbs").getAsBoolean();
			report = jo.get("report").getAsBoolean();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("[EventRecieveScreen] Invalid json from: " + (playerNick == null ? ia.getHostAddress() : playerNick) + " dropped.");
			this.setCanceled(true);
		}

	}

	public String getPlayerNick() {
		return playerNick;
	}

	public String getToken() {
		return token;
	}

	public String getAdminNick() {
		return adminNick;
	}

	public boolean isWbs() {
		return wbs;
	}

	public boolean isReport() {
		return report;
	}

	public JsonObject getJo() {
		return jo;
	}


}
