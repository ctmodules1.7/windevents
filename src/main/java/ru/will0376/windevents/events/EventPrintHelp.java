package ru.will0376.windevents.events;

import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import ru.will0376.windevents.utils.Logger;

public class EventPrintHelp extends Event {
	private final ICommandSender playerMP;

	public EventPrintHelp(ICommandSender player) {
		playerMP = player;
	}

	public ICommandSender getPlayer() {
		return playerMP;
	}

	public void printToPlayer(String text) {
		if (playerMP.equals("Server")) {
			Logger.log(1, "", text);
			return;
		}
		if (playerMP == null) {
			Logger.log(1, "[EPrintHelp]", "Wtf, player == null!");
			return;
		}
		playerMP.addChatMessage(new ChatComponentText(text));
	}
}
