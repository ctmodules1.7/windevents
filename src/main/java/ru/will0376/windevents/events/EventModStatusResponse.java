package ru.will0376.windevents.events;

import cpw.mods.fml.common.eventhandler.Event;

public class EventModStatusResponse extends Event {
	String modid;
	boolean enabled;

	public EventModStatusResponse(String modid, boolean enabled) {
		this.modid = modid;
		this.enabled = enabled;
	}

	public String getModid() {
		return modid;
	}

	public boolean isEnabled() {
		return enabled;
	}
}
