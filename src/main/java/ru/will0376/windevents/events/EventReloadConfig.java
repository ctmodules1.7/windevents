package ru.will0376.windevents.events;

import cpw.mods.fml.common.eventhandler.Event;

public class EventReloadConfig extends Event {
	private final String sender;

	public EventReloadConfig(String sender) {
		this.sender = sender;
	}

	public String getSender() {
		return sender;
	}
}
