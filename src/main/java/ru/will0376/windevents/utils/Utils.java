package ru.will0376.windevents.utils;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;
import ru.will0376.windevents.events.EventLogPrint;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class Utils {
	public static boolean checkPermission(ICommandSender sender, String permkey) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, "[CheckPermission]", "Check perm: " + permkey + " for sender: " + sender.getCommandSenderName()));
		if (sender.canCommandSenderUseCommand(4, "ct.allperms") || sender.canCommandSenderUseCommand(4, permkey) || sender.getCommandSenderName().equals("Server"))
			return true;
		else {
			sender.addChatMessage(new ChatComponentText(ChatForm.prefix_error + EnumChatFormatting.RED + "You do not have the right to command!"));
			return false;
		}
	}

	public static EntityPlayerMP getEPMP(String nick) {
		for (EntityPlayerMP player : Utils.getAllPlayers())
			if (player.getDisplayName().equals(nick))
				return player;
		return null;
	}

	public static List<EntityPlayerMP> getAllPlayers() {
		return FMLCommonHandler.instance().getMinecraftServerInstance().getConfigurationManager().playerEntityList;
	}

	/*
	*
		public static String usage = Ctreport.MOD_ID +": {\n" +
			" /"+Ctreport.config.getMainCommand() + " <player>\n" +
			"\n" +
			" Aliases: ["+String.join(", ", Ctreport.config.getAliases())+"]\n" +
			" Permissions: [\n" +
			"   ctmodules.report.admin.cooldownbypass\n" +
			"   ctmodules.report.admin.reportwlbypass\n" +
			"   ctmodules.report.admin.blacklist\n" +
			"  ]\n" +
			"}";
	* */
	public static String makeUsage(String modid, String usage, String aliases, String permissions) {
		return modid + ": {\n" +
				usage +
				"\n" +
				aliases +
				"\n" +
				permissions +
				"\n}"
				;
	}

	public static void printWithCase(ICommandSender sender, String prefix, List<String> list) {
		AtomicBoolean ab = new AtomicBoolean(false);
		list.forEach(e -> {
			if (!ab.getAndSet(true)) sender.addChatMessage(new ChatComponentText(prefix + e));
			else sender.addChatMessage(new ChatComponentText(e));
		});
	}

	public static List<String> fromCaseToList(String text, String splitter) {
		return Arrays.asList(text.split(splitter));
	}

	private BufferedImage convertStringToBufferedImage(String image) {
		byte[] imageInByte = image.getBytes();

		String ii = image;
		InputStream in = new ByteArrayInputStream(imageInByte);
		BufferedImage bImageFromConvert = null;
		try {

			bImageFromConvert = ImageIO.read(in);

			return bImageFromConvert;
		} catch (IOException e) {
		}
		return null;
	}
}
