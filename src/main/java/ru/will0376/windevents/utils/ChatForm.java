package ru.will0376.windevents.utils;


import net.minecraft.util.EnumChatFormatting;

public class ChatForm {
	public static final String prefix = EnumChatFormatting.GOLD + "[ClientTweaker]: " + EnumChatFormatting.RESET;
	public static final String report_prefix = EnumChatFormatting.GOLD + "[Report]: " + EnumChatFormatting.RESET;
	public static final String prefix_error = EnumChatFormatting.RED + "[ClientTweaker_error]: " + EnumChatFormatting.RESET;
	public static final String report_prefix_error = EnumChatFormatting.RED + "[Report_error]: " + EnumChatFormatting.RESET;
	public static final String prefix_error_whitelist = EnumChatFormatting.RED + "[ClientTweaker_error_whitelist]: " + EnumChatFormatting.RESET;
}

