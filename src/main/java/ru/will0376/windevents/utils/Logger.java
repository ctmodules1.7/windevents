package ru.will0376.windevents.utils;


import net.minecraftforge.common.MinecraftForge;
import ru.will0376.windevents.events.EventLogPrint;

public class Logger {
	/**
	 * @deprecated Данный метод чисто для поддержки старого кода. используй Logger#log
	 */
	public static void printWithName(int mode, String domain, String text) {
		log(mode, domain, text);
	}

	public static void log(int mode, String domain, String text) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(mode, domain, text));
	}
}
